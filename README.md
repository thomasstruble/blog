# Data Science Decoded

### Contributing

This blog is built with hugo. To contribute, begin by creating a new post:

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/mikedoesdatascience/blog.git)

```bash
$ make new-post POST="my-new-post"
Content "/workspace/blog/site/content/posts/my-new-post/index.md" created
Switched to a new branch 'my-new-post'
```

This will also create a new local git branch. On this branch, edit your newly created blog post content. You can see the rendered version by starting a local development server. It is recommended to use GitPod for which there is a make target ready to go:

```bash
make gitpod-server
```

Then you can open the preview in your browser, and changes will update live.

When you're done writing your blog post, push your branch to GitLab and create a merge request. Tag a reviewer and wait for comments! When the MR is accepted and merged into the main branch, a CI job will be trigged to update the deployed blog.