---
title: "Introduction"
date: 2023-04-30T13:33:53Z
toc: false
images:
tags:
  - intro
  - contributing
---

#### Introduction to the blog

Hello All and welcome to the collaborative data science blog. This started as an idea to have a blog that multiple people can contribute to so that there is not a single person responsible for their own blog. 

Checkout the About page for more background information and follow the rules of the blog that are outlined there. 

Here we will describe how to contribute:

The code is located here https://gitlab.com/mikedoesdatascience/blog

The easiest method to deploy is to click on the open in gitpod link and sign up for an account or link your GitHub/GitLab account. 

![Gitpod-image](gitpod-example.jpeg)

Once you have the VSCode editor open you can use the terminal at the bottom to create you blog post using the make instructions in the README.md in the repository.

![Gitpod-vscode](gitpod-vscode.jpeg)


Have fun contributing! Thomas and Mike

 

